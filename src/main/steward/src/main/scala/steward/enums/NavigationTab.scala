package steward.enums

object NavigationTab extends Enumeration {
  val TOPICS: NavigationTab.Value = Value("Topics")
  val HISTORY: NavigationTab.Value = Value("History")
  val STATISTICS: NavigationTab.Value = Value("Statistics")
}