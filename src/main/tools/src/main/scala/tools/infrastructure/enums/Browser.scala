package tools.infrastructure.enums

/**
  * Represents different browsers to be used to run the tests.
  */
object Browser extends Enumeration {
  val CHROME, FIREFOX, SAFARI, IE, EDGE = Value
}